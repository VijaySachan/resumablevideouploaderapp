import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavController, AlertController, LoadingController} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File ,DirectoryEntry,FileEntry, IFile} from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { VideoEditor } from '@ionic-native/video-editor';
import { Uploader } from "./Uploader";
import { ChatService } from '../../app/chat.service';
import { Base64 } from '@ionic-native/base64';
import { DomSanitizer } from '@angular/platform-browser';
import { BackgroundModeHelper } from './BackgroundModeHelper';
import { BackgroundMode } from '@ionic-native/background-mode';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private uploader:Uploader
  private backgroundModeHelper:BackgroundModeHelper
  constructor(
    private backgroundMode: BackgroundMode,
    private base64: Base64,
    private chatService:ChatService,
    private videoEditor: VideoEditor,
    public navCtrl: NavController, private camera: Camera,
    private transfer: FileTransfer, private file: File,
    private sanitizer: DomSanitizer,
    private alertCtrl: AlertController, private loadingCtrl: LoadingController
    ) {
      console.log("constructor....");
      
      this.isUploadButtonDisabled=true
      this.isStopUploadingButtonDisabled=true
      //
      this.backgroundModeHelper=new BackgroundModeHelper(backgroundMode)
      //
      this.uploader=new Uploader(chatService,this.backgroundModeHelper,this);

  }
  ;
  isSelectButtonDisabled=false
  isUploadButtonDisabled=true
  isStopUploadingButtonDisabled=true
  labelFileName="--"
  public extraInfoText="------------"
  picToView:any="assets/imgs/logo.png"
  ///-----
  public stopUploading=false
  selectedVideoUrl: string; //= "https://res.cloudinary.com/demo/video/upload/w_640,h_640,c_pad/dog.mp4";
  uploadedVideo: string;

  isUploading: boolean = false;
  uploadPercent: number = 0;
  videoFileUpload: FileTransferObject;
  loader;
  showLoader() {
    this.loader = this.loadingCtrl.create({
      content: 'Please wait..'
    });
    this.loader.present();
  }

  dismissLoader() {
    this.loader.dismiss();
  }
  setLoadingText(text:string) {
    const elem = document.querySelector(
          "div.loading-wrapper div.loading-content");
    if(elem) elem.innerHTML = text;
  }
  presentAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['Dismiss']
    });
    alert.present();

  }
 
  // selectVideo() {
  //   console.log("testConnection");
  //   this.chatService.socket.emit("Start", { fileName: "123", size: 5 });
  // }
  selectVideo() {
    this.mLog("Inside selectVideo ")
    this.extraInfoText="---"
    this.stopUploading=false
    const options: CameraOptions = {
      quality: 100,
      // destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.VIDEO
    }
     this.camera.getPicture(options)
      .then( async (videoUrl) => {
        if(videoUrl) {
          this.selectedVideoUrl=videoUrl;
          this.showLoader();
          var filename = videoUrl.substr(videoUrl.lastIndexOf('/') + 1);
          this.labelFileName=filename
          this.displayImageThumbnail(videoUrl);
          
       } 
      },
      (err) => {
        console.log(err);
        this.presentAlert("Oops","Error occurred ");
      });
 }
mLog(msg:string){
console.log(msg);
}
async displayImageThumbnail(videoUrl:string){
    var videoFileName = '-thumbnail'; 
    this.videoEditor.createThumbnail(
        {
            fileUri: videoUrl,
            outputFileName: videoFileName,
            atTime: 1,
            // width: 320, 
            // height: 480,
            quality: 100
        }
    ).then(async (imageURL)=>{
      console.log(imageURL)
      var filename = imageURL.substr(imageURL.lastIndexOf('/') + 1);
      var dirpath = imageURL.substr(0, imageURL.lastIndexOf('/') + 1);
      this.mLog("dirpath1 -"+dirpath);
      dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;
      try {
        this.mLog("dirpath2 --"+dirpath);
        this.mLog("filename - "+filename);
        await  this.file.resolveDirectoryUrl(dirpath)
.then((directoryEntry: DirectoryEntry) => {
   this.file.getFile(directoryEntry, filename, { create: false })
  .then((fileEntry: FileEntry) => {
    fileEntry.file((file:IFile)=>{
      var reader = new FileReader();
    reader.onload =  (encodedFile: any)=>{
      var src = reader.result;
      this.picToView = src;  
      this.isUploadButtonDisabled=false
       this.dismissLoader();
       
      }
    reader.readAsDataURL(file); 
    });
  });
    });
  }catch(err) {
   console.error(err)
   this.presentAlert("Oops","Error creating video thumbnail");
  }})
}
uploadVideoButtonClicked(){
  this.stopUploading=false;
  this.isSelectButtonDisabled=true;
  this.isUploadButtonDisabled=true
  this.isStopUploadingButtonDisabled=false;
// this.showLoader();
// this.checkFileToCompress()
this.prepareToUploadVideo(this.selectedVideoUrl)


}
stopUploadVideoButtonClicked(){
  this.stopUploading=true;
  this.isSelectButtonDisabled=false;
  this.isUploadButtonDisabled=false;
  this.isStopUploadingButtonDisabled=true
  }
async prepareToUploadVideo(videoUrl) {
  this.extraInfoText="Starting upload"
  var filename = videoUrl.substr(videoUrl.lastIndexOf('/') + 1);
  var dirpath = videoUrl.substr(0, videoUrl.lastIndexOf('/') + 1);
  // this.mLog("dirpath1 -"+dirpath);
    dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;
          try {
            // this.mLog("dirpath2 -- "+dirpath);
            // this.mLog("filename - "+filename);
           await this.file.resolveDirectoryUrl(dirpath)
  .then((directoryEntry: DirectoryEntry) => {
    this.file.getFile(directoryEntry, filename, { create: false })
      .then((fileEntry: FileEntry) => {
        fileEntry.file((file:IFile)=>{
          // this.setLoadingText("Uploading");
          this.dismissLoader();
          this.uploader.setData(file,filename);
          this.backgroundModeHelper.startBackgroundMode()
          this.uploader.StartUpload()
         console.log("ready to upload")
        });
      });
  }).catch(error => {
    console.log("resolveDirectoryUrl error "+error)
  })
    ;
} catch(err) {
            this.dismissLoader();
            console.log("eee "+err)
            return this.presentAlert("prepareToUploadVideo","Something went wrong."+err);
          }
}
 checkFileToCompress(){ 
   
  var videoUrl=this.selectedVideoUrl
  var filename = videoUrl.substr(videoUrl.lastIndexOf('/') + 1);
  var compressedFileName="comp_"+filename;
  var dirpath = videoUrl.substr(0, videoUrl.lastIndexOf('/') + 1);
  this.mLog("dirpath1 -"+dirpath);
    dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;
    var tempurl=videoUrl.includes("file://") ? videoUrl : "file://" + videoUrl;
  // Compress file now
 var promise=this.videoEditor.transcodeVideo({
  fileUri: tempurl,
  outputFileName: compressedFileName,
  outputFileType: this.videoEditor.OutputFileType.MPEG4,
  progress:	(info: number) =>{
    var num=Math.round(info*100)
   this.extraInfoText="Compressing video "+num+" %"
   
  }
})
.then((fileUri: string) => {
  console.log('video transcode success', fileUri)
   this.prepareToUploadVideo(fileUri)
}
)
.catch((error: any) => {console.log('video transcode error'+ error)
// this.dismissLoader();
this.presentAlert("Oops","video transcode error ")
}
);

// promise.catch((error: any) => {console.log('video transcode error'+ error)
// // this.dismissLoader();
// this.presentAlert("Oops","stopped ")
// })  

}

public uploadCompleted(){
  this.isSelectButtonDisabled=false
  this.isStopUploadingButtonDisabled=true
  this.isUploadButtonDisabled=true
}

}