import { Component } from '@angular/core';
import { BackgroundMode } from '@ionic-native/background-mode';

// import { Socket } from 'ng-socket-io';
export class BackgroundModeHelper {

    constructor(private backgroundMode: BackgroundMode) {
//         
//                 this.backgroundMode.on('activate').subscribe(() => {
// console.log("activated")
// this.updateNotification()

//      });
    
    }
     startBackgroundMode(){
        this.backgroundMode.setDefaults({
                        title: "Title",
                text: "text",
                        resume: true,
                        icon: 'ic_launcher'
                            })
         this.backgroundMode.overrideBackButton()
         this.backgroundMode.excludeFromTaskList()
         this.backgroundMode.enable()
     }
     stopBackgroundMode(){
         this.backgroundMode.disable()
     }
     isBackgroundModeActive():boolean{
return this.backgroundMode.isActive()
     }

     updateNotification(title,text){
        
            this.backgroundMode.configure({
                title: title,
              text: text
       });
    
        
    
     }
}