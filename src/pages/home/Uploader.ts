// import { File } from '@ionic-native/file';
import { Component, OnInit } from '@angular/core';
import io from "socket.io-client";
import { HttpClient } from '@angular/common/http';
import { FileEntry ,IFile} from '@ionic-native/file';
import { ChatService } from '../../app/chat.service';
import { BackgroundModeHelper } from './BackgroundModeHelper';
import { HomePage } from './home';
// import { Socket } from 'ng-socket-io';
export class Uploader  {
  title = "resumeUpload-ui";
  public selectedFile:IFile;
  fReader:FileReader;
  public name = "";
  uploadPercent;
  color = "primary";
  mode = "determinate";
  socket;
constructor(chatService:ChatService,private backgroundModeHelper:BackgroundModeHelper,private home:HomePage) {
    this.socket=chatService.socket
    this.handleResponse();
  }
  setData(selectedFile:IFile,name:string) {
          this.selectedFile=selectedFile
    this.name=name
      }
  handleResponse() {
    this.socket.on('MoreData', (data)=>{
      
      this.handleMoreData(data);
      });
      
      
      
      this.socket.on('Done', (data)=>{
        this.handleUploadCompleted(data);
      });
  }
handleUploadCompleted(data){
  var Content = "Video Successfully Uploaded! !"
        var text="Uploaded 100 %"
        this.home.extraInfoText=text
        this.backgroundModeHelper.updateNotification(this.name,text)
        // this.backgroundModeHelper.stopBackgroundMode()
        this.home.uploadCompleted()
        console.log(Content);
}

handleMoreData(data){
  console.log('\nmore Data:-', data)
      console.log('\nmore mozSlice:', this.selectedFile)
      var text="Uploaded "+Math.round(data['Percent'])+" %"
      this.home.extraInfoText=text
      this.backgroundModeHelper.updateNotification(this.name,text)
      //  this.UpdateBar(data['Percent']);
        var Place = data['Place'] * 524288; //The Next Blocks Starting Position
        var NewFile; //The Variable that will hold the new Block of Data
        // if(this.selectedFile.webkitSlice)
        //     NewFile = this.selectedFile.webkitSlice(Place, Place + Math.min(524288, (this.selectedFile.size-Place)));
        // else
            NewFile = this.selectedFile.slice(Place, Place + Math.min(524288, (this.selectedFile.size-Place)));
            // NewFile = SelectedFile.mozSlice(Place, Place + Math.min(524288, (SelectedFile.size-Place)));
        this.fReader.readAsBinaryString(NewFile);
}
  goToLink(url: string){
    window.open(url, "_blank");
  }

//   onFileSelect(event) {
//     this.selectedFile = event.target.files[0];
//     this.name = this.selectedFile.name;
//     console.log(this.selectedFile);
//   }

 StartUpload(){
      this.fReader = new FileReader();
      this.fReader.onload = (evnt)=>{
        if (!this.home.stopUploading){
          this.socket.emit('Upload', { Name : this.name, Data : this.fReader.result}); 
      }else{
        this.socket.emit('WriteData', { Name : this.name}); 
      }
      }
      this.socket.emit('Start', { Name : this.name, Size : this.selectedFile.size });
  }
  print(){
      console.log("yay");
  }
   UpdateBar(percent){
    // document.getElementById('ProgressBar').style.width = percent + '%';
    // document.getElementById('percent').innerHTML = (Math.round(percent*100)/100) + '%';
    // var MBDone = Math.round(((percent/100.0) * SelectedFile.size) / 1048576);
    // document.getElementById('MB').innerHTML = MBDone;
  }

  
  
}

