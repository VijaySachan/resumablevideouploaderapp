import * as io from 'socket.io-client';

export class ChatService {
    // private url = 'http://172.20.10.2:3001';
    private url = 'http://localhost:3001';
    public socket;    

    constructor() {
        this.socket = io(this.url);
        console.log("ChatService constructor");
    }
}